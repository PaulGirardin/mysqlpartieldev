<?php
/**
 * Created by PhpStorm.
 * User: yahiko
 * Date: 12/12/2015
 * Time: 12:12
 */

namespace App\Controller\Front;

use Core\Controller\Controller;

class Users_auditsController extends AppController
{
    public function __construct()
    {
        //On appelle le constructeur parent
        parent::__construct();
        $this->loadModel('Users');
        $this->loadModel('Users_audits');
    }

    // liste tous nos articles de la page d'index
    public function all()
    {
        $audits = $this->Users_audits->allDDesc();
        $users = $this->Users->all();
        //On construit un tableau à partir des variables avec la fonction compact
        $this->render('users_audits.index', compact('audits', 'users'));

    }
}
