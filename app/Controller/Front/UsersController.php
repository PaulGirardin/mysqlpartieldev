<?php
/**
 * Created by PhpStorm.
 * User: yahiko
 * Date: 12/12/2015
 * Time: 13:36
 */
namespace App\Controller\Front;

use Core\Auth\DbAuth;
use \App;
use Mail;

class UsersController extends AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel('Users');
    }


    public function login()
    {
        $errors = false;
        if(!empty($_POST)){
            $auth = new DbAuth(App::getInstance()->getDb());

            if($auth->login($_POST['username'], sha1($_POST['password']))){
                header('Location: index.php');
            } else {
                $errors = true;
                $this->render('users.login', compact('errors'));
            }
        } else {
            $this->render('users.login', compact('errors'));
        }
    }


    public function logout()
    {
        session_unset();
        header('Location:index.php');
    }


    public function register()
    {
        // Check if already logged
        if(isset($_SESSION["auth"])){
            if(isset($_GET["redirect"])){
                header("Location: index.php?" . $_GET["redirect"]);
            } else {
                header("Location: index.php");
            }
        } else {
            $error = "";

            if (isset($_POST) && !empty($_POST)) {
                if ($_POST["login_password"] === $_POST["login_password1"]) {
                    $auth = new DbAuth(App::getInstance()->getDb());

                    if(!isset($_POST["type"])){
                        $_POST["type"] = 1;
                    }

                    $error = $auth->register($_POST["login_username"], sha1($_POST["login_password"]), $_POST["login_email"], $_POST["type"]);

                    if ($error === "inserted") {
                        $auth->login($_POST['login_username'], sha1($_POST['login_password']));
                        header("Location: index.php");
                    }
                } else {
                    $error = "password";
                }
            }

            $this->render("users.register", compact("error"));
        }
    }


    public function all()
    {
        $users = $this->Users->all();
        $this->render("users.all", compact("users"));
    }


    public function edit()
    {
        if(isset($_GET["id"])){
            if (isset($_POST) && !empty($_POST)) {
                $result = $this->Users->update($_GET["id"], "id", ["name" => $_POST["name"],"mail" => $_POST["mail"]]);

                if($result){
                    if($_GET["id"] == $_SESSION["auth"]){
                        $_SESSION["pseudo"] = $_POST["name"];
                    }

                    header("Location: index.php?p=users.all");
                } else {
                    echo "RATE";
                }
            }

            $user = $this->Users->userAsked($_GET["id"]);
            $this->render("users.edit", compact("user"));
        } else {
            header("Location: index.php");
        }
    }


    public function delete(){
        if(isset($_GET["id"]) && !empty($_GET["id"])){
            if($_SESSION["auth"] == $_GET["id"]){
                session_unset();
            }

            $this->Users->delete($_GET["id"],"id");
        }

        header("Location: index.php?p=users.all");
    }


}
