<?php

namespace App\Controller\Front;

use Core\Auth\DbAuth;
use \App;

/**
 *
 */
class HomeController extends AppController
{

    public function __construct()
    {
        parent::__construct();
        // This is how you load model
        // $this->loadModel('users');
    }

    public function index()
    {
        // $users = $this->Users->all();
        // require(ROOT . "/app/Views/home/index.php");
        $this->render("home.index");
    }

}
