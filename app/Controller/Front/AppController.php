<?php
/**
 * Created by PhpStorm.
 * User: yahiko
 * Date: 12/12/2015
 * Time: 12:35
 */

namespace App\Controller\Front;

use Core\Controller\Controller;
use \App;

class AppController extends Controller
{
    //par défaut est à default
    protected $template = 'default';

    //défini le view Path
    public function __construct()
    {
        $this->viewPath = ROOT . '/app/Views/';
    }


    public function loadModel($model_name)
    {
        $this->$model_name = App::getInstance()->getTable($model_name);
    }


}

