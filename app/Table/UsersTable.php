<?php
namespace App\Table;
use Core\Table\Table;

// All the tables of the app extends this one
class UsersTable extends Table
{
    //TODO: insérer le nom de la table des utilisateurs
    protected $table = "users";

    /**
     *Récupère les users
     * @return array
     */
    public function all()
    {
        return $this->query("
                    SELECT *
                    FROM $this->table
                    ORDER BY name ASC");
    }

    /**
     *Récupère l'utilisateur demandé
     * @return array
     */
    public function userAsked($id)
    {
        return $this->query("
                    SELECT *
                    FROM $this->table
                    WHERE `id`=" . $id, null, true);
    }

    /**
     *Récupère les clients
     * @return array
     * @param type = 3 -> administrateur
     */
    public function allAdmins()
    {
        return $this->query("
                    SELECT *
                    FROM $this->table
                    WHERE type=3
                    ORDER BY name ASC");
    }
}