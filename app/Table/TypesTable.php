<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 15/12/2015
 * Time: 16:07
 */

namespace App\Table;
use Core\Table\Table;


class TypesTable extends Table
{
    protected $table = "oetzi_types";


    /**
     *Récupère les styles
     * @return array
     */
    public function all()
    {
        return $this->query("
                    SELECT *
                    FROM oetzi_types");
    }

}