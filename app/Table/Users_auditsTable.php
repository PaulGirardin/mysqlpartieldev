<?php
namespace App\Table;

use Core\Table\Table;

class Users_auditsTable extends Table
{
    //Récupère le nom de la classe
    protected $table = "users_audits";

    public function allDDesc()
    {
        return $this->query("
                    SELECT *
                    FROM $this->table
                    ORDER BY date DESC");
    }

}
