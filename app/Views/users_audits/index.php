<table>
    <tr>
        <th class="firsttd">#</th>
        <th class="secondtd">Username</th>
        <th class="thirdtd">Action</th>
    </tr>
        <?php $i = 0;foreach ($audits as $audit): $i++; ?>
            <tr>
                <td class="firsttd"><?= $i ?></td>
                <td class="secondtd">
                    <?php if(isset($users[$audit->user_id]->name)) {
                        echo $users[$audit->user_id]->name;
                    } else {
                        echo "User deleted";
                    }
                    ?>
                </td>
                <td class="thirdtd"><?= $audit->action; ?> the <?= $audit->date; ?></td>
            </tr>
        <?php endforeach; ?>

</table>
