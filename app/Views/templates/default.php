<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= App::getInstance()->getTitle(); ?></title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="images/mustachos.ico" />
</head>
<body>
    <div id="header">
        <a href="index.php">INDEX</a>

        <?php if(isset($_SESSION["auth"])){  ?>
            <text id="profile">
                Bonjour <?= $_SESSION["pseudo"] ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="?p=users.logout">LOGOUT</a>
            </text>
        <?php } ?>
    </div>

    <!-- Display all content -->
    <div id="content">
        <?= $content ?>
    </div>

</body>
</html>