<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 15/12/2015
 * Time: 16:06
 */

namespace App\Entite;

use Core\Entite\Entite;


class UsersEntite extends Entite
{
    public function getUrl()
    {
        return 'index.php?p=styles.single&id=' . $this->usersId;
    }

    public function getTitle()
    {
        $title = $this->titre;

        return $title;
    }
}