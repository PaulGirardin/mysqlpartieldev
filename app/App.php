<?php

use Core\Config;
use Core\Database\MysqlDatabase;
// Class static pour remplacer la variable $db
//Permet de tout gérer
class App
{
    public static $title = "Partiel dev MySQL";
    private static $database;
    private static $_instance;
    private $db_instance;

    // Get the currently running instance
    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = new App();
        }

        return self::$_instance;
    }


    public static function load()
    {
        session_start();
        require ROOT . '/app/Autoloader.php';
        //Permet de lancer les Views de notre application
        App\Autoloader::register();
        require ROOT . '/core/Autoloader.php';
        //Charge les classes
        Core\Autoloader::register();
    }

    public static function getTitle()
    {
        return self::$title;
    }

    public function getTable($name){
        $class_name = "\\App\\Table\\" . ucfirst($name) . "Table";

        return new $class_name($this->getDb());
    }

    public function getDb(){
        //On passe un fichier de configuration de la BDD
        $config = Config::getInstance(ROOT . '/config/config.php');
        if(is_null($this->db_instance)){
            $this->db_instance = new MysqlDatabase($config->get("db_name"), $config->get("db_host"), $config->get("db_pass"), $config->get("db_user"));
        }

        return $this->db_instance;
    }

    /**
     * setter du titre
     * @param $title
     */
    public static function setTitle($title)
    {
        self::$title = self::$title . $title;
    }

}
