<?php
/**
 * Created by PhpStorm.
 * User: yahiko
 * Date: 09/12/2015
 * Time: 12:15
 */
namespace Core\Auth;

use Core\Database\Database;

class DbAuth
{
    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function getUserId()
    {
        if($this->logged()){
            return $_SESSION["auth"];
        }

        return false;
    }

    /**
     * @param $username = nom utilisateur
     * @param $password = mot de passe
     * @return boolean
     */
    public function login($username, $password)
    {
        $user = $this->db->prepare('SELECT * FROM users WHERE name= ?', [$username], null, true);

        if($user){
            if($user->password === ($password)){
                $_SESSION["auth"] = $user->id;
                $_SESSION["pseudo"] = $username;
                $_SESSION["type"] = $user->type;

                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }

    public function logged()
    {
        return isset($_SESSION['auth']);
    }

    public function logout()
    {
        session_unset();
        header('Location:index.php');
    }


    /**
     * @param $username
     * @param $password
     * @param $email
     * @param $type
     * @return bool
     */
        public function register($username, $password, $email, $type)
        {
            $user = $this->db->prepare('SELECT id FROM users WHERE name = ?', [$username], null, true);

            if(!$user){
                  $mail = $this->db->prepare('SELECT id FROM users WHERE mail = ?', [$email], null, true);
                // Si le mail existe déjà, return mail en tant qu'erreur
                  if(!$mail){
                        $this->db->query("INSERT INTO users (`name`, `password`, `mail`, `type`) VALUES ('$username', '$password', '$email', '$type')");

                        return "inserted";
                  } else{
                        return "mail";
                  }
            }

            // user existe déjà
            return "user";
        }

}
