<?php

namespace Core\Database;

use \PDO;

class MysqlDatabase extends Database
{

    private $db_name;
    private $db_user;
    private $db_pass;
    private $db_host;
    private $pdo;

    /**
     * Database constructor.
     * @param $db_name
     * @param $db_host
     * @param $db_pass
     * @param $db_user
     */
    public function __construct($db_name, $db_host = 'localhost', $db_pass = 'root', $db_user = 'root')
    {
        $this->db_name = $db_name;
        $this->db_host = $db_host;
        $this->db_pass = $db_pass;
        $this->db_user = $db_user;
    }

    /**
     * Instancie la classe en PDO et la stock dans l'objet
     * Gère une seule fois la connexion à la base de données
     */
    private function getPDO()
    {
        if($this->pdo === null){
            $pdo = new PDO('mysql:dbname=' . $this->db_name . ';host=' . $this->db_host,
                $this->db_user,
                $this->db_pass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
        }

        return $this->pdo;
    }


    public function query($statement, $class_name = null, $one = false)
    {
        $req = $this->getPDO()->query($statement);
        if(
            strpos($statement, 'UPDATE') === 0 ||
            strpos($statement, 'INSERT') === 0 ||
            strpos($statement, 'DELETE') === 0
        ) {
            return $req;
        }
        if($class_name === null){
            $req->setFetchMode(PDO::FETCH_OBJ);
        } else {
            $req->setFetchMode(PDO::FETCH_CLASS, $class_name);
        }
        if($one){
            $datas = $req->fetch();
        } else {
            $datas = $req->fetchAll();
        }

        return $datas;
    }


    /**
     * @param $statement = requête sql
     * @param $attributes = options supplémentaires liées aux erreurs
     * @param $class_name = le nom de la classe
     * @param $one = return un tableau avec un ou plusieurs résultats
     * @return array
     */
    public function prepare($statement, $attributes, $class_name = null, $one = false)
    {
        $req = $this->getPDO()->prepare($statement);
        $res = $req->execute($attributes);
        if(
            strpos($statement, 'UPDATE') === 0 ||
            strpos($statement, 'INSERT') === 0 ||
            strpos($statement, 'DELETE') === 0
        ) {
            return $res;
        }
        if($class_name === null){
            $req->setFetchMode(PDO::FETCH_OBJ);
        } else {
            $req->setFetchMode(PDO::FETCH_CLASS, $class_name);
        }
        if($one){
            $datas = $req->fetch();
        } else {
            $datas = $req->fetchAll();
        }

        return $datas;
    }


    public function lastInsertID(){
        return $this->getPDO()->lastInsertId();
    }
}
