<?php
namespace Core\Table;

use \Core\Database\Database;


class Table
{
    protected $table;
    protected $db;

    public function __construct(Database $db){
        $this->db = $db;
        if(is_null($this->table)) {
            $parts = explode("\\", get_class($this));
            $class_name = end($parts);
            $this->table = strtolower(str_replace("table", "", $class_name)) . 's';
        }
    }

    public function all(){
        return $this->query("SELECT * FROM " . $this->table);
    }

    public function query($statement, $attributes = null, $one = false)
    {
        if($attributes){
            return $this->db->prepare(
                $statement,
                $attributes,
                str_replace('Table', 'Entite', get_class($this)),
                $one);
        } else {
            return $this->db->query(
                $statement,
                str_replace('Table', 'Entite', get_class($this)),
                $one);
        }
    }

    public function find($id){
        return $this->query("SELECT * FROM " . $this->table . " WHERE `id` = " . $id, null, true);
    }

    public function update($id, $primary, $fields)
    {
        $sqlParts = [];
        //On stock les modifications dans un tableau
        $attributes = [];
        foreach ($fields as $field => $f) {
            $sqlParts[] = "$field = ?";
            $attributes[] = $f;
        }
        $attributes[] = $id;
        $sqlPart = implode(', ', $sqlParts);

        return $this->query("UPDATE " . $this->table . " SET " . $sqlPart . " WHERE " . $primary . " = ?", $attributes, true);
    }

    public function create($fields)
    {
        $sqlParts = [];
        //On stock les modifications dans un tableau
        $attributes = [];
        foreach ($fields as $field => $f) {
            $sqlParts[] = "$field = ?";
            $attributes[] = $f;
        }
        $sqlPart = implode(', ', $sqlParts);

        $table = str_replace("tables", "", $this->table);

        return $this->query("INSERT INTO " . $table . " SET " . $sqlPart, $attributes, true);
    }

    public function delete($id, $primary)
    {
        return $this->query("DELETE FROM " . $this->table . " WHERE `" . $primary . "` = " . $id);
    }
}
