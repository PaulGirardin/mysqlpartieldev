<?php
namespace Core\Entite;

class Entite
{

    /**
     * Fonction magique
     * @param $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        $method = 'get' . ucfirst($key);
        $this->$key = $this->$method();

        return $this->$key;
    }
}