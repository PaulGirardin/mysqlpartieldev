<?php
/**
 * Created by PhpStorm.
 * User: yahiko
 * Date: 12/12/2015
 * Time: 12:10
 */

namespace Core\Controller;

class Controller
{
    protected $viewPath;
    protected $template;

    //aficher les vues
    protected function render($view, $variables = [])
    {
        ob_start();
        extract($variables);
        require ($this->viewPath . str_replace('.', '/', $view) . '.php');
        $content = ob_get_clean();

        require ($this->viewPath . 'templates/' . $this->template . '.php');
    }

    protected function forbidden()
    {
        //TODO Faire la page de redirection
        /*header('HTTP/1.0 403 Forbidden');
        die('Accès interdit');*/
        header("Location: index.php");
    }

    protected function notFound()
    {
        //TODO Faire la page de redirection
        header('HTTP/1.0 404 Not Found');
        die('Page Introuvable');
    }
}
