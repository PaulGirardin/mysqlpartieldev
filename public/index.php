<?php
// error_reporting(0);
// ini_set('display_errors', 0);
header('Content-Type: text/html; charset=utf-8');
define('ROOT', dirname(__DIR__));
require ROOT . '/app/App.php';
App::load();

$app = App::getInstance();

// Session is for Facebook
if(isset($_GET['p'])){
    $_SESSION["p"] = $_GET["p"];
    $page = $_GET['p'];
} else {
    $_SESSION["p"] = "home.index";
    $page = 'home.index';
}

$page = explode('.', $page);

if($page[0] == 'admin'){
    $controller = '\App\Controller\Admin\\' . ucfirst($page[1]) . 'Controller';
    $action = $page[2];
} else {
    $controller = '\App\Controller\Front\\' . ucfirst($page[0]) . 'Controller';
    $action = $page[1];
}
$controller= new $controller();
$controller->$action();
